const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    await db.dropCollection('categories');
    await db.dropCollection('products');
    await db.dropCollection('users');

    const [categoryCars, categoryComputers, categoryOthers] = await Category.create({
            title: 'Car',
            description: 'Cars and auto parts'
        }, {
            title: 'Computers',
            description: 'Computers, notebooks, phones'
        },
        {
            title: 'Others',
            description: 'All that was not included in the main sections'
        });

    const [userIgor, userAlex, userJohn] = await User.create(
        {
            fio: "Igor",
            phone: "1234562",
            name: "Nagibator6000)))",
            password: "123"
        }, {
            fio: "Alex",
            phone: "9876543221",
            name: "AlexSuperZver",
            password: "123"
        },
        {
            fio: "John",
            phone: "1234223",
            name: "Evgeniy",
            password: "123"
        });

    await Product.create({
            title: 'Mercedes',
            price: 300000,
            description: 'Mercedes GLK 500 2017year ',
            category: categoryCars._id,
            image: 'mers.jpg',
            author: userIgor._id
        }, {
            title: 'Acer',
            price: 110000,
            description: 'Acer Predator',
            category: categoryComputers._id,
            image: 'acer.jpg',
            author: userAlex._id
        },
        {
            title: 'Ural',
            price: 190000,
            description: 'The best of the best velosipéd :)',
            category: categoryOthers._id,
            image: 'bike.jpg',
            author: userJohn._id
        });

    db.close();
});