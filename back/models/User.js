const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const UserSchema = new Schema({
    fio: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if (!this.isModified('displayName')) return true;
                const user = await User.findOne({displayName: value});
                if (user) throw new Error('This user already exist');
                return true;
            },
            message: 'This username already exist'
        }
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if (!this.isModified('phone')) return true;
                const user = await User.findOne({phone: value});
                if (user) throw new Error('This phone already exist');
                return true;
            },
            message: 'This phone already exist'
        }

    },
    token: String
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;

    next();
});


UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.set('toJSON', {
    transform: (doc, ret, option) => {
        delete ret.password;
        return ret;
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;