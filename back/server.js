const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const port = 8000;
const config = require('./config');

const users = require('./app/users');
const categories = require('./app/categories');
const products = require('./app/products');



app.use(express.json());
app.use(cors());
app.use(express.static('public'));
const db = mongoose.connection;
mongoose.connect(config.db.url + '/' + config.db.name);


db.once('open',()=>{
    app.use('/users', users());
    app.use('/categories', categories());
    app.use('/products', products());


    app.listen(port,()=>{
        console.log(`Server started on ${port} port!`);
    })
});