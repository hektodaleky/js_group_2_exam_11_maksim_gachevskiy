import {FETCH_ONE_PRODUCTS_SUCCESS, FETCH_PRODUCTS_SUCCESS, GET_CATEGOTIES} from "../actions/actionTypes";

const initialState = {
    products: [],
    currentProduct: {},
    categories: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case FETCH_ONE_PRODUCTS_SUCCESS:
            return {...state, currentProduct: action.currentProduct};
        case GET_CATEGOTIES:{
            return {...state, categories: action.categories}}
        default:
            return state;
    }
};

export default reducer;