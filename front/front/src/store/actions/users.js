import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";
import axios from "../../axios-api";
const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS}
};

const registerUserFailure = (error) => {
    return {type: REGISTER_USER_FAILURE, error}
};

export const registerUser = userData => {
    return dispatch => {
        axios.post('/users', userData).then(
            response => {

                dispatch(registerUserSuccess());
                dispatch(push('/'))
            },
            error => dispatch(registerUserFailure(error.response.data))
        )
    };
};

const loginUserSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = error => {
    return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(

            response => {
                console.log(response.data);
                dispatch(loginUserSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                const errorObject = error.response ? error.response.data : {error: "Not Internet"}
                dispatch(loginUserFailure(errorObject));
            }
        )
    }
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete('/users/sessions', {headers}).then(
            response => {
                dispatch({type: LOGOUT_USER});
                dispatch(push('/'))
            }
        );
    }
};
