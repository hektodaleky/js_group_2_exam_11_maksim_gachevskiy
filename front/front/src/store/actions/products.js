import axios from "../../axios-api";
import {
    CREATE_PRODUCT_SUCCESS, FETCH_ONE_PRODUCTS_SUCCESS, FETCH_PRODUCTS_SUCCESS,
    GET_CATEGOTIES
} from "./actionTypes";
import {push} from "react-router-redux";


export const fetchProductsSuccess = products => {
    return {type: FETCH_PRODUCTS_SUCCESS, products};
};
export const fetchOneProductsSuccess = currentProduct => {
    return {type: FETCH_ONE_PRODUCTS_SUCCESS, currentProduct};
};

export const getCategoties = categories=>{
    return {type: GET_CATEGOTIES, categories}
};

export const fetchProducts = () => {
    return dispatch => {
        axios.get('/products').then(
            response => {
                return dispatch(fetchProductsSuccess(response.data))
            }
        );
    }
};
export const fetchProductsCategory = (id) => {
    return dispatch => {
        axios.get('/products?category='+id).then(
            response => {
                return dispatch(fetchProductsSuccess(response.data))
            }
        );
    }
};

export const fetchCategories = () => {
    return dispatch => {
        axios.get('/categories').then(
            response => {
                return dispatch(getCategoties(response.data))
            }
        );
    }
};

export const fetchOneProducts = (id) => {

    return dispatch => {
        axios.get('/products?id='+id).then(
            response => {
                dispatch(fetchOneProductsSuccess(response.data));
                dispatch(push('/products?id='+id));


            }
        );
    }
};

export const createProductSuccess = () => {
    return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = (productData,token) => {
    return dispatch => {
        return axios.post('/products',productData,{headers: {'Token': token}}).then(response=>{
            console.log(response);
                dispatch(push('/'));
        }

        );
    };
};

export const deleteProduct = (token,product) => {
    console.log(token,product);
    return dispatch => {
        return axios.delete('/products/' + product, {headers: {'Token': token}})
            .then(response => {
                dispatch(push('/'));


            },err=>{
                alert("Вы не можете удалить чужой товар")
            });
    }
};