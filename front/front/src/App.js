import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";

import MainMenu from "./containers/MainMenu/MainMenu";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import ProductInfo from "./containers/ProductInfo/ProductInfo"
import ProductForm from "./containers/ProductForm/ProductForm";

class App extends Component {
    render() {
        return (

            <Layout>
                <Switch>
                    <Route path="/" exact component={MainMenu}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/products" component={ProductInfo}/>
                    <Route path="/newproduct" exact component={ProductForm}/>

                </Switch>
            </Layout>
        );
    }
}

export default App;
