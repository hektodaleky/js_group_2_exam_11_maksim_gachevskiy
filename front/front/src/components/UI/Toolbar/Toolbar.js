import React from "react";
import {Nav, Navbar} from "react-bootstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user,logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>

                {user ? <UserMenu logout={logout} user={user.name}/> : <AnonymousMenu />}


            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;