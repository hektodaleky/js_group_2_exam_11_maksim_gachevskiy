import React from "react";
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const AnonymousMenu = () => (
    <Nav>
        <LinkContainer to="/register" exact>
            <NavItem>Sign up</NavItem>
        </LinkContainer>
        <LinkContainer to="/login" exact>
            <NavItem>Login</NavItem>
        </LinkContainer>

    </Nav>
);

export default AnonymousMenu;