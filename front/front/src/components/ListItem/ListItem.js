import React from "react";
import {Button, Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import PropTypes from "prop-types";

import config from "../../config";

import notFound from "../../assets/images/not-found.png";

const ListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + 'uploads/' + props.image;
    }

    return (



        <Card sm="6">
            {props.image !== 'hide' ? <CardImg top width="100%" src={image} alt="Artist Image"
                                               style={{width: '100px', marginRight: '10px'}}/> : null}
            <CardBody>
                <CardTitle> {props.num ? props.num + ". " + props.name : props.name}</CardTitle>
                <CardText>{props.about}</CardText>
                {props.buttonName? <Button onClick={props.click}>{props.buttonName}</Button>:null}
                {props.optionalField? <CardSubtitle>{props.optionalField}</CardSubtitle>:null}
            </CardBody>
        </Card>



    );
};

ListItem.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    click: PropTypes.func,
    buttonName: PropTypes.string,
    num: PropTypes.number,
    optionalField: PropTypes.string
};

export default ListItem;