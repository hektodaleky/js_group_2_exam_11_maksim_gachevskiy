import React, {Component} from "react";
import {connect} from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import {CardColumns} from "reactstrap";
import {fetchCategories, fetchOneProducts, fetchProducts, fetchProductsCategory} from "../../store/actions/products";

class MainMenu extends Component {
    componentDidMount() {
        this.props.fetchProducts();
        this.props.fetchCategories();

    };




    render() {
        console.log(this.props.products);
        return (

            <CardColumns>
                <div style={{margin:"30px"}}>
                    {this.props.category.map(item=>{

                            return <p className="navig"
                                      key={item._id}
                                      onClick={()=>this.props.fetchProductsCategory(item._id)}>{item.title}</p>
                    })}
                </div>
            {
                this.props.products.map(product=>{
                    return <ListItem key={product._id}
                                     about={product.price}
                                     name={product.title}
                                     image={product.image}
                                     id={product._id}
                                     click={() =>this.props.fetchOneProducts(product._id) }
                                     buttonName="details"

                    />
                })
            }
        </CardColumns>)
    }
}
;
const mapStateToProps = (state) => ({
    products: state.products.products,
    category: state.products.categories,
});

const mapDispatchToProps = dispatch => ({
    fetchProducts:()=>dispatch(fetchProducts()),
    fetchOneProducts:(id)=>dispatch(fetchOneProducts(id)),
    fetchCategories: () => dispatch(fetchCategories()),
    fetchProductsCategory:(id)=>dispatch(fetchProductsCategory(id))


});
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);