import React, {Component} from "react";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {connect} from "react-redux";
import {createProduct, fetchCategories} from "../../store/actions/products";

class ProductForm extends Component {
    componentDidMount() {
        this.props.fetchCategories();
    }



    state = {
        title: '',
        price: '',
        description: '',
        image: '',
        category: ''
    };

    sendData=(event)=>{
        event.preventDefault();


        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createProduct(formData,this.props.user.token)
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        console.log(this.props.category);
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="productTitle">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text" required
                            placeholder="Enter product title"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productPrice">
                    <Col componentClass={ControlLabel} sm={2}>
                        Price
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="number" min="0" required
                            placeholder="Enter product price"
                            name="price"
                            value={this.state.price}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>


                <FormGroup controlId="productCategory">
                    <Col componentClass={ControlLabel} sm={2}>
                        Category
                    </Col>
                    <Col sm={10}>
                        <select onChange={this.inputChangeHandler} name="category" size="4" style={{fontSize: "12px"}}>
                            {
                                this.props.category.map(item => {
                                    return <option key={item._id} value={item._id}>{item.title}</option>

                                })
                            }
                        </select>
                    </Col>
                </FormGroup>


                <FormGroup controlId="productDescription">
                    <Col componentClass={ControlLabel} sm={2}>
                        Description
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            componentClass="textarea"
                            placeholder="Enter description"
                            name="description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.sendData} bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    category: state.products.categories,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchCategories()),
    createProduct:(item,token)=>dispatch(createProduct(item,token))

});
export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);

