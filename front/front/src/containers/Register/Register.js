import React, {Component, Fragment} from "react";

import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import Alert from "react-bootstrap/es/Alert";
import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions/users";

class Register extends Component {
    state = {
        name: '',
        password: '',
        fio: '',
        phone:''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser(this.state);
    };

    fieldHasError = fieldName => {
        return this.props.error && this.props.error.errors[fieldName]
    };


    render() {
        let error = null;
        if (this.props.error) {
            error = <Alert bsStyle="danger">ОПАСНОСТЬ!!!</Alert>
        }
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                {error}
                <Form horizontal onSubmit={this.submitFormHandler}>


                    <FormElement propertyName="name"
                                 title="name"
                                 placeholder="Enter username"
                                 type="text"
                                 value={this.state.name}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"
                                 error={this.fieldHasError('name') && this.props.error.errors.name.message}/>

                    <FormElement propertyName="password"
                                 title="Password"
                                 placeholder="Enter password"
                                 type="text"
                                 value={this.state.password}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-password"
                                 error={this.fieldHasError('password') && this.props.error.errors.password.message}/>
                    <FormElement propertyName="fio"
                                 title="Full Name"
                                 placeholder="Enter Full Name"
                                 type="text"
                                 value={this.state.fio}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-fio"
                                 error={this.fieldHasError('fio') && this.props.error.errors.password.message}/>
                    <FormElement propertyName="phone"
                                 title="Phone"
                                 placeholder="Enter phone"
                                 type="text"
                                 value={this.state.phone}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-phone"
                                 error={this.fieldHasError('phone') && this.props.error.errors.password.message}/>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }

}
;
const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
