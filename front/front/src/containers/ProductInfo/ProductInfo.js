import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {Button, Image, Jumbotron} from "react-bootstrap";
import parser from "../../parser";
import {deleteProduct, fetchOneProducts} from "../../store/actions/products";
import config from "../../config";
import notFound from "../../assets/images/not-found.png";


class ProductInfo extends Component{
    componentDidMount(){

        if (!this.props.product.title)
            this.props.fetchOneProducts(parser(window.location.search).id);

    };

    render(){
        let image= notFound;
        if (this.props.product.image) {
            image = config.apiUrl + 'uploads/' + this.props.product.image;
        }
        return (
            this.props.product.title?<Jumbotron>
                <h1>{this.props.product.title}</h1>
                <h2>{"Category: "+this.props.product.category.title}</h2>
                {image  ? <Image width="500px" src={image} alt=""
                                                   style={{width: '500px', marginRight: '10px'}}/> : null}
                <p>
                    {"Name: "+this.props.product.author.fio}
                </p>
                <p>
                    {"Phone: "+this.props.product.author.phone}
                </p>

                {this.props.user?
                    <p>
                        <Button onClick={()=>this.props.deleteProduct(this.props.user.token, this.props.product._id)} bsStyle="primary">Delete</Button>
                    </p>:null}

            </Jumbotron>:null

        )
    }

};
const mapStateToProps = (state) => ({
    product: state.products.currentProduct,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({

    fetchOneProducts:(id)=>dispatch(fetchOneProducts(id)),
    deleteProduct:(token,id)=>dispatch(deleteProduct(token,id))

});
export default connect(mapStateToProps, mapDispatchToProps)(ProductInfo);